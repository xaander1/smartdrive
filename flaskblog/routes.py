import os
import secrets
import glob
import textract
import difflib
import string
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from PIL import Image
from flask import render_template, url_for, flash, redirect, request, abort,send_from_directory
from flaskblog import app, db, bcrypt
from flaskblog.forms import RegistrationForm, LoginForm, UpdateAccountForm, PostForm
from flaskblog.models import User, Post
from flask_login import login_user, current_user, logout_user, login_required
from cltk.text_reuse.comparison import long_substring


@app.route("/")
@app.route("/home", methods=['GET', 'POST'])
def home():
    posts = Post.query.all()
    return render_template('home.html', posts=posts)


@app.route("/about")
def about():
    return render_template('about.html', title='About')


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your account has been updated!', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account',
                           image_file=image_file, form=form)



def save_document(form_document):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_document.filename)
    document_fn = random_hex + f_ext
    document_path = os.path.join(app.root_path, 'static/documents', document_fn)
    form_document.save(document_path)
    return document_fn

def save_zip(form_zip):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_zip.filename)
    zip_fn = random_hex + f_ext
    zip_path = os.path.join(app.root_path, 'static/zip', zip_fn)
    form_zip.save(zip_path)
    return zip_fn
def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos=set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text



def det_plag(current_document):
    fil = glob.glob("/home/alexander/Develop/pyScratch/project II-(sample-working)/flaskblog/static/documents/*.pdf")
    large = 0
    #try:
    for x in fil:
        current_link = '/home/alexander/Develop/pyScratch/project II-(sample-working)/flaskblog/static/documents/%s'%(current_document) 
        changing_link = '%s'%(x)
        if(current_link == changing_link):
            continue
        else:
            current_text = ''.join(convert_pdf_to_txt(current_link).strip().split('\n\n'))
            changing_text = ''.join(convert_pdf_to_txt(changing_link).strip().split('\n\n'))
            ration = fuzz.ratio(current_text, changing_text)
            if(ration > large):
                large=ration
                high_plag_pdf = os.path.basename(changing_link)
                # matches = difflib.SequenceMatcher(None, current_text, changing_text).get_matching_blocks()
                current_textx = str(current_text) 
                changing_textx = str(changing_text)
                # for match in matches:
    final_content = "" 
       #     matching_content = current_text[match.a:match.a + match.size]
    text_x = str(current_textx)
    text_z = str(changing_textx)
    matching_content= long_substring(current_textx, changing_textx) 
    while len(matching_content) > 15:   
        #if(len(matching_content) > 3 ):
        working_with = '<section class="bg-danger" >%s</section>'%(matching_content)
        working_with_z = '<section class="bg-primary">%s</section>'%(matching_content)
        text_x = '<section class="mt-2" >%s</section>'%(str.replace(text_x, matching_content,working_with))
        text_x = text_x.strip()
        text_z = '<section class="mt-2" >%s</section>'%(str.replace(text_z, matching_content,working_with_z))
        text_z = text_z.strip()
        current_textx = current_textx.replace(matching_content,"")
        changing_textx = changing_textx.replace(matching_content,"").strip()
        print("i ran")
        matching_content = long_substring(current_textx, changing_textx)

        flag = True
                        #final_content = final_content + '\n' + matching_content
    else:
            if (flag == False):
                text_x = current_textx
                text_z = changing_textx
                print("i didnt")
    return (large,high_plag_pdf,text_x,text_z)
    #except:
    #    return (0,os.path.basename(current_link),"None")

@app.route("/post/new", methods=['GET', 'POST'])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        document_name = save_document(form.document.data)
        (percent,plag_pdf,edited_a,edited_b) = det_plag(document_name)
        zip_name = save_zip(form.zipfile.data)
        post = Post(title=form.title.data, content=form.content.data, author=current_user,
            document_file=document_name,zip_file=zip_name,plag_percent = percent,pdf_plag=plag_pdf,edited_x = edited_a,edited_z = edited_b)
        db.session.add(post)
        db.session.commit()
        flash('Your post has been created!', 'success')
        return redirect(url_for('home'))
    return render_template('create_post.html', title='New Post',
                           form=form, legend='New Post')


@app.route("/post/<int:post_id>")
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)


@app.route("/post/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        flash('Your post has been updated!', 'success')
        return redirect(url_for('post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('create_post.html', title='Update Post',
                           form=form, legend='Update Post')


@app.route("/post/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Your post has been deleted!', 'success')
    return redirect(url_for('home'))

@app.route('/download/<path:filename>', methods=['GET', 'POST'])
def download(filename):  
        document_path = os.path.join(app.root_path, 'static/documents')
        return send_from_directory(directory=document_path, filename=filename)
@app.route('/download2/<path:filename>', methods=['GET', 'POST'])
def download2(filename):  
        document_path = os.path.join(app.root_path, 'static/zip')
        return send_from_directory(directory=document_path, filename=filename)